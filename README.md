# annotatex-scripts
Various scripts for backup, extraction and manteinance for the AnnotateX project.

The AnnotateX project is comprised of 3 repositories: `com_annotatex` is the Joomla! extension
for the tool, `tpl_annotatex` is the Joomla! template and this repository which contains
some useful scripts.

## What's in Here?
Most of the queries that we use for the extraction are saved in this repository.
If the query was really simple I didn't include it here to not pollute the repository with
useless information.

Never use these queries as they are. Always assume you need customization. The queries
contain hardcoded user ids and conditions you may want to check and modify.

Under the `node` folder there's a Node.js script for the insertion of new x-ray dicoms into
the AnnotateX database. Don't execute it blindly as it may need some modifications, too.
Depending on you SSH configuration you may need to change the connection parameters.

AnnotateX is currently installed on `apmup.lnx.warwick.ac.uk`. There's a MySQL server, that can be accessed with:

```
mysql dbjoomla -uannotate_user -pAnnChestXray19
```

From here you can execute the SQL scripts.

## Configuration
The scripts work assuming you have configured your system properly.
Create the file `~.ssh/config` and add the following lines:

```
Host annotatex
     HostName apmup.lnx.warwick.ac.uk
	 User u1874286
```

Make sure to put your login in the `User` field, not mine.

Now run `ssh-copy-id annotatex` and then check if everything went right with `ssh annotatex`.

## Automatic Report Upload
Before starting make a backup. Things can go wrong in every moment.

The following steps are required to upload new reports.

1. Create a new report collection
2. Upload the new reports

### Create a New Report Collection
To create a new report collection use the template `eg_insert-collection.sql`.
Remember to read and edit the script and not execute it directly.

### Upload the New Reports
First, make sure the CSV file does not contain an header line.

Given a file name and a collection id, execute the following:

```
nodejs insert-report.js FILE COLLECTION_ID | ssh annotatex "mysql dbjoomla -uannotate_user -pAnnChestXray19"
```

The previous line works assuming you have `ssh-copy-id` the host `annotatex`.

## Automatic X-Ray upload
The following steps are required to upload new chest X-Rays.

1. Create a new x-ray collection
2. Upload new DICOMS
3. Assign an X-Ray to a collection
4. Add prelabels to the x-rays

### Create a New X-Ray Collection
To create a new x-ray collection you can use the SQL script `eg_insert-collection.sql`.

### Uploading New DICOMS
There's a bash script that given a CSV file it will upload all the DICOMs and will insert all the records
in `ax_xray` table (but not in `ax_labelled_xray`, which can be done with a query inside the node folder).

Remember to strip out the headers from the CSV file.

Always check the script, don't run it blindly. Take a backup first. To run the script:

```
./upload-xrays.sh
```

### Assign an X-Ray to a Collection
There are two scripts for doing that: `template_insert-labelled-xray-abnormal.sql` and
`template_insert-labelled-xray-normal.sql`. The scripts have to be modified with the correct ids.

### Add Prelabels to X-Rays
Use the script `template_insert-xray-labelling.sql`.
