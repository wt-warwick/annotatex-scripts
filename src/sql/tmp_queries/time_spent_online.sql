SELECT user                                                                                      AS user_id,
        date(ax_xray_submission.load_time)                                                       AS day,
        SUM(TIMESTAMPDIFF(SECOND, ax_xray_submission.load_time, ax_xray_submission.submit_time))/60/24 AS hours_spent
FROM ax_xray_submission, ax_labelled_xray, ax_xray_collection
WHERE ax_labelled_xray.xray = ax_xray_submission.xray
AND ax_xray_collection.id = ax_labelled_xray.collection
AND ax_labelled_xray.collection = ax_xray_submission.collection
and user in (715, 716) -- (Ashik, Keerthini)
GROUP BY user, ax_xray_collection.id, day;