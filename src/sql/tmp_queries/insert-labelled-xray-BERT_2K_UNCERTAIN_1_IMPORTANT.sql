INSERT INTO ax_labelled_xray (xray, collection, report_annotators)
SELECT _sq.id,
       11, -- PUT HERE THE ID OF THE NEW COLLECTION
       _sq.report_annotators
FROM (
    SELECT report.id                                                                                   AS id,
           GROUP_CONCAT(DISTINCT annotation_label.label ORDER BY annotation_label.label SEPARATOR '#') AS labels,
	   COUNT(DISTINCT submission.user)                                                             AS report_annotators
    FROM ax_report AS report
    INNER JOIN ax_submission  AS submission
    ON report.id = submission.report
    INNER JOIN ax_annotation AS annotation
    ON submission.id = annotation.submission
    INNER JOIN ax_annotation_label AS annotation_label
    ON annotation.id = annotation_label.annotation
    WHERE submission.submit_time >= ALL (
        SELECT MAX(submit_time)
        FROM ax_submission
        WHERE ax_submission.report = submission.report
        AND ax_submission.user = submission.user
	AND DATE(ax_submission.submit_time) < (
	    SELECT create_date
	    FROM ax_xray_collection
	    WHERE id = 11-- PUT HERE THE ID OF THE NEW COLLECTION
	))
    AND annotation_label.label NOT IN (
        'possible_diagnosis',
        'other',
        'recommendation',
        'comparison',
        'technical_issue',
        'undefined_sentence')
    AND annotation_label.label IN ( -- "important" labels
        'parenchymal_lesion',
        'paratracheal_hilar_enlargement',
        'paraspinal_mass',
        'pneumothorax',
        'volume_loss',
        'rib_lesion',
        'ground_glass_opacification',
        'consolidation',
        'interstitial_shadowing')
    AND report.id IN (
    	SELECT ax_collection_report.report
	FROM ax_collection_report
	WHERE ax_collection_report.collection in (12,13) -- PUT HERE THE ID OF THE ***REPORT COLLECTION***
    )
    GROUP BY report.id
    HAVING labels <> 'normal')
AS _sq;
