select s.*, a.*, al.* -- DISTINCT(s.report), 36 -- 
from ax_submission as s,
ax_annotation as a,
ax_annotation_label as al
where s.id = a.submission
and a.id = al.annotation
and a.normalized_sentence like "%no pneumothorax%"
and al.label = 'pneumothorax';
