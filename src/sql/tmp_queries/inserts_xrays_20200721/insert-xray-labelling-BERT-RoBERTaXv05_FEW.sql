INSERT INTO ax_xray_labelling (xray, collection, label)
SELECT DISTINCT labelled_xray.xray AS xray,
                labelled_xray.collection AS collection,
                labelling.label AS label
FROM ax_labelled_xray AS labelled_xray
INNER JOIN (
    SELECT submission.report AS report,
           annotation_label.label AS label
    FROM ax_submission AS submission
    INNER JOIN ax_annotation AS annotation
    ON submission.id = annotation.submission
    INNER JOIN ax_annotation_label AS annotation_label
    ON annotation.id = annotation_label.annotation
    WHERE submission.submit_time >= ALL (
        SELECT MAX(submit_time)
        FROM ax_submission
        WHERE ax_submission.report = submission.report
        AND ax_submission.user = submission.user
	AND DATE(ax_submission.submit_time) < (
	    SELECT create_date
	    FROM ax_xray_collection
	    WHERE id = 13 -- PUT HERE THE ID OF THE XRAY COLLECTION
	))
    AND annotation_label.label NOT IN (
	    'comparison',
        'normal',
        'other',
	    'possible_diagnosis',	    
	    'recommendation',
	    'technical_issue',
        'undefined_sentence')) AS labelling
ON labelled_xray.xray = labelling.report
WHERE labelled_xray.collection IN (
    13 -- PUT HERE THE ID OF THE XRAY COLLECTION
)
ORDER BY labelled_xray.xray;
