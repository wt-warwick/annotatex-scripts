SELECT _sq.id,
       12, -- PUT HERE THE ID OF THE NEW COLLECTION
       _sq.report_annotators
FROM (
    SELECT report.id                                                                                   AS id,
           GROUP_CONCAT(DISTINCT annotation_label.label ORDER BY annotation_label.label SEPARATOR '#') AS labels,
	   COUNT(DISTINCT submission.user)                                                             AS report_annotators
    FROM ax_report AS report
    INNER JOIN ax_submission  AS submission
    ON report.id = submission.report
    INNER JOIN ax_annotation AS annotation
    ON submission.id = annotation.submission
    INNER JOIN ax_annotation_label AS annotation_label
    ON annotation.id = annotation_label.annotation
    WHERE submission.submit_time >= ALL (
        SELECT MAX(submit_time)
        FROM ax_submission
        WHERE ax_submission.report = submission.report
        AND ax_submission.user = submission.user
	AND DATE(ax_submission.submit_time) < (
	    SELECT create_date
	    FROM ax_xray_collection
	    WHERE id = 12 -- PUT HERE THE ID OF THE NEW COLLECTION
	))
    AND annotation_label.label NOT IN (
        'possible_diagnosis',
        'other',
        'recommendation',
        'comparison',
        'technical_issue',
        'undefined_sentence')
    AND report.id IN (SELECT ax_collection_report.report
                    FROM ax_collection_report
                    WHERE ax_collection_report.collection not in (14,21)
                    AND ax_collection_report.collection >= 12 -- From BERT
                    AND ax_collection_report.collection <= 35 -- To RoBERTaXv05
                    )
    AND report.id not in (select xray from ax_xray_submission) -- not submitted
    AND report.id not in (select xray from ax_pending_xray) -- not pending
    AND report.id not in (select id from ax_xray) -- not uploaded previously
    GROUP BY report.id
    HAVING labels = 'normal')
AS _sq;