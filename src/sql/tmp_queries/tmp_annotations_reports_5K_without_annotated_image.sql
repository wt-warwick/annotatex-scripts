SELECT CONCAT(submission.report,        '$',
	      al.title)
AS 'report_id$label'
FROM ax_submission AS submission
INNER JOIN ax_annotation AS annotation
ON submission.id = annotation.submission
INNER JOIN ax_annotation_label AS annotation_label
ON annotation.id = annotation_label.annotation
INNER JOIN jmt_user_usergroup_map AS usergroup_map
ON submission.user = usergroup_map.user_id
INNER JOIN ax_collection_report AS acr
ON acr.report = submission.report
INNER JOIN ax_label as al
ON label = al.id
WHERE usergroup_map.group_id = 10 -- This is the 'Ragiologist' group
AND submission.`submit_time` = (
    SELECT MAX(`submit_time`)
    FROM `ax_submission`
    WHERE `user` = submission.user
    AND `report` = submission.report)
AND acr.collection IN (12, 13, 15, 16, 17, 19)
GROUP BY submission.report, al.title
ORDER BY submission.report,
         al.title;