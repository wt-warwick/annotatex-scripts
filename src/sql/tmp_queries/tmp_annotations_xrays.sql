SELECT CONCAT(submission.xray,  '$',
              submission.user,  '$',
	      submission.id,    '$',
              al.title, '$',
	      'BB',             '$',
              annotation.shapes)
AS 'xray_id$radiologist_id$submission_id$label$ROI_type$ROI_shape'
FROM ax_xray_annotation AS annotation
INNER JOIN ax_xray_submission AS submission
ON annotation.submission = submission.id
INNER JOIN jmt_user_usergroup_map AS usergroup_map
ON submission.user = usergroup_map.user_id
INNER JOIN ax_label as al
ON annotation.label = al.id
WHERE usergroup_map.group_id = 10 -- This is the 'Radiologist' group
AND submission.submit_time >= (
    SELECT MAX(submit_time)
    FROM ax_xray_submission
    WHERE ax_xray_submission.user = submission.user
    AND ax_xray_submission.xray = submission.xray
    AND ax_xray_submission.collection = submission.collection);
