SELECT CONCAT(submission.report,        '$',
          al.id,        '$',
	      al.title)
AS 'report_id$label$label_title'
FROM ax_submission AS submission
INNER JOIN ax_annotation AS annotation
ON submission.id = annotation.submission
INNER JOIN ax_annotation_label AS annotation_label
ON annotation.id = annotation_label.annotation
INNER JOIN jmt_user_usergroup_map AS usergroup_map
ON submission.user = usergroup_map.user_id
INNER JOIN ax_collection_report AS acr
ON acr.report = submission.report
INNER JOIN ax_label as al
ON label = al.id
WHERE usergroup_map.group_id = 10 -- This is the 'Ragiologist' group
AND submission.`submit_time` = (
    SELECT MAX(`submit_time`)
    FROM `ax_submission`
    WHERE `user` = submission.user
    AND `report` = submission.report)
AND acr.collection not in (14, 21)
AND acr.collection >= 12 -- From BERT_2K
AND acr.collection <= 25 -- To RoBERTaX_v02
AND submission.report not in (select xray from ax_xray_submission) -- not submitted
AND submission.report not in (select xray from ax_pending_xray) -- not pending
GROUP BY submission.report, al.title
ORDER BY submission.report,
         al.title;