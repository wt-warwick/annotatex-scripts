select alx.xray
from ax_labelled_xray as alx,
     ax_xray_assignment as axa
where axa.collection = alx.collection
and axa.user = 716
and axa.collection = 20
and alx.xray not in (select axs.xray
                    from ax_xray_submission as axs
                    where axs.user = axa.user
                    and axs.collection = axa.collection)
and alx.xray not in (select apx.xray
                    from ax_pending_xray as apx
                   where apx.user = axa.user)

