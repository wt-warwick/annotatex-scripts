SELECT CONCAT(ax_collection_report.report, ',',
              ax_collection.id, ',',
              ax_collection.title) AS 'report_id,collection_id,collection'
FROM ax_collection_report,
    ax_collection
WHERE ax_collection.id = ax_collection_report.collection
