SELECT axsb.xray
FROM (SELECT axs.collection, axs.user, axs.xray
        FROM ax_xray_submission axs
        WHERE axs.xray NOT IN (SELECT xray FROM ax_pending_xray)) AS axsb,
    jmt_users AS ju,
    jmt_user_usergroup_map AS juum,
    (SELECT axc.title, axc.id, count(1) AS 'total'
        FROM ax_labelled_xray alx, ax_xray_collection axc
        WHERE alx.collection = axc.id
        GROUP BY axc.id) axtot
WHERE axsb.user = ju.id
AND ju.id = juum.user_id
AND juum.group_id = 10
AND axtot.id = axsb.collection
GROUP BY axsb.xray;