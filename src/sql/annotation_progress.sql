SELECT CONCAT(ju.id, ',',
              ju.username, ',',
              ac.id, ',',
              ac.title, ',',
              progress.total, ',',
              progress.completed, ',') AS 'user_id,username,collection_id,collection,total,completed'
FROM (SELECT total.user,
        total.coll_id,
        total.total,
        IFNULL(done.completed, 0) AS 'completed'
    FROM (SELECT aa.user,
        aa.collection AS 'coll_id',
        cacr.coll_count AS 'total'
        FROM ax_assignment AS aa,
            (SELECT acr.collection AS coll_id,
            count(1) AS coll_count
            FROM ax_collection_report AS acr
            GROUP BY acr.collection) AS cacr
        WHERE aa.collection = cacr.coll_id
        GROUP BY aa.user, aa.collection) AS total
    LEFT JOIN
        (SELECT aa.user,
                aa.collection AS 'coll_id',
                count(gasb.report) AS 'completed'
        FROM ax_assignment AS aa,
                ax_collection_report AS acr,
                (SELECT asb.user,
                        asb.report
                FROM ax_submission AS asb
                GROUP BY asb.user, asb.report) AS gasb
        WHERE aa.collection = acr.collection
        AND aa.user = gasb.user
        AND acr.report = gasb.report
        GROUP BY aa.user, aa.collection) AS done
    ON (total.user = done.user
        AND total.coll_id = done.coll_id)) AS progress,
    ax_collection AS ac,
    jmt_users AS ju,
    jmt_user_usergroup_map AS juum
WHERE ac.id = progress.coll_id
AND ju.id = progress.user
AND ju.id = juum.user_id
-- AND ju.block = 0
AND juum.group_id = 10
ORDER BY ju.username, ac.title;
