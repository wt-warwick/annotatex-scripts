SELECT left_table.username AS username,
       left_table.report   AS report,
       left_table.sentence AS sentence,
       left_table.labels   AS labels
FROM (
    SELECT user_left.username                                                                           AS username,
           submission_left.report                                                                       AS report,
           CONCAT('"', REPLACE(`annotation_left`.`normalized_sentence`, '"', '""'), '"')                AS sentence,
           GROUP_CONCAT(annotation_label_left.label ORDER BY annotation_label_left.label SEPARATOR '|') AS labels
    FROM ax_latest_submission AS submission_left
    INNER JOIN jmt_users AS user_left
    ON submission_left.user = user_left.id
    INNER JOIN ax_annotation AS annotation_left
    ON submission_left.id = annotation_left.submission
    INNER JOIN ax_annotation_label AS annotation_label_left
    ON annotation_left.id = annotation_label_left.annotation
    WHERE user_left.username = 'c.lim'
    AND submission_left.report IN (
        SELECT ax_submission.report
	FROM ax_submission
	INNER JOIN jmt_users
	ON ax_submission.user = jmt_users.id
	WHERE jmt_users.username = 'c.horst') -- 2nd user
    GROUP BY username,
             report,
	     sentence)
AS left_table
LEFT OUTER JOIN (
    SELECT user_left.username                                                                           AS username,
           submission_left.report                                                                       AS report,
           CONCAT('"', REPLACE(`annotation_left`.`normalized_sentence`, '"', '""'), '"')                AS sentence,
           GROUP_CONCAT(annotation_label_left.label ORDER BY annotation_label_left.label SEPARATOR '|') AS labels
    FROM ax_latest_submission AS submission_left
    INNER JOIN jmt_users AS user_left
    ON submission_left.user = user_left.id
    INNER JOIN ax_annotation AS annotation_left
    ON submission_left.id = annotation_left.submission
    INNER JOIN ax_annotation_label AS annotation_label_left
    ON annotation_left.id = annotation_label_left.annotation
    WHERE user_left.username = 'c.horst' -- 2nd user
    AND submission_left.report IN (
        SELECT ax_submission.report
	FROM ax_submission
	INNER JOIN jmt_users
	ON ax_submission.user = jmt_users.id
	WHERE jmt_users.username = 'c.lim')
    GROUP BY username,
             report,
	     sentence) AS right_table
ON left_table.report = right_table.report
AND left_table.sentence = right_table.sentence
WHERE left_table.labels <> right_table.labels
AND left_table.report in (select report from ax_collection_report where collection = 21) -- collection id

UNION

SELECT left_table.username AS username,
       left_table.report   AS report,
       left_table.sentence AS sentence,
       left_table.labels   AS labels
FROM (
    SELECT user_left.username                                                                           AS username,
           submission_left.report                                                                       AS report,
           CONCAT('"', REPLACE(`annotation_left`.`normalized_sentence`, '"', '""'), '"')                AS sentence,
           GROUP_CONCAT(annotation_label_left.label ORDER BY annotation_label_left.label SEPARATOR '|') AS labels
    FROM ax_latest_submission AS submission_left
    INNER JOIN jmt_users AS user_left
    ON submission_left.user = user_left.id
    INNER JOIN ax_annotation AS annotation_left
    ON submission_left.id = annotation_left.submission
    INNER JOIN ax_annotation_label AS annotation_label_left
    ON annotation_left.id = annotation_label_left.annotation
    WHERE user_left.username = 'c.horst' -- 2nd user
    AND submission_left.report IN (
        SELECT ax_submission.report
	FROM ax_submission
	INNER JOIN jmt_users
	ON ax_submission.user = jmt_users.id
	WHERE jmt_users.username = 'c.lim')
    GROUP BY username,
             report,
	     sentence)
AS left_table
LEFT OUTER JOIN (
    SELECT user_left.username                                                                           AS username,
           submission_left.report                                                                       AS report,
           CONCAT('"', REPLACE(`annotation_left`.`normalized_sentence`, '"', '""'), '"')                AS sentence,
           GROUP_CONCAT(annotation_label_left.label ORDER BY annotation_label_left.label SEPARATOR '|') AS labels
    FROM ax_latest_submission AS submission_left
    INNER JOIN jmt_users AS user_left
    ON submission_left.user = user_left.id
    INNER JOIN ax_annotation AS annotation_left
    ON submission_left.id = annotation_left.submission
    INNER JOIN ax_annotation_label AS annotation_label_left
    ON annotation_left.id = annotation_label_left.annotation
    WHERE user_left.username = 'c.lim'
    AND submission_left.report IN (
        SELECT ax_submission.report
	FROM ax_submission
	INNER JOIN jmt_users
	ON ax_submission.user = jmt_users.id
	WHERE jmt_users.username = 'c.horst') -- 2nd user
    GROUP BY username,
             report,
	     sentence) AS right_table
ON left_table.report = right_table.report
AND left_table.sentence = right_table.sentence
WHERE left_table.labels <> right_table.labels
AND left_table.report in (select report from ax_collection_report where collection = 21) -- collection id