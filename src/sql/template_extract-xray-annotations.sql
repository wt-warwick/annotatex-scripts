SELECT CONCAT(submission.xray,       '$',
              submission.collection, '$',
              annotation.label,      '$',
              annotation.shapes)
FROM ax_xray_annotation AS annotation
INNER JOIN ax_xray_submission AS submission
ON annotation.submission = submission.id
WHERE submission.submit_time >= (
    SELECT MAX(submit_time)
    FROM ax_xray_submission
    WHERE ax_xray_submission.user = submission.user
    AND ax_xray_submission.xray = submission.xray
    AND ax_xray_submission.collection = submission.collection);
