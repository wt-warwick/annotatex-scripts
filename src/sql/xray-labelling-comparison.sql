SELECT CONCAT(prelabelling.xray, ',', prelabelling.collection, ',"', prelabelling.labelling, '","', postlabelling.labelling, '"')
FROM (
    SELECT labelled.xray                                                                     AS xray,
           labelled.collection                                                               AS collection,
           GROUP_CONCAT(DISTINCT labelling.label ORDER BY labelling.label ASC SEPARATOR ',') AS labelling
    FROM ax_labelled_xray AS labelled
    INNER JOIN ax_xray_labelling AS labelling
    ON labelled.xray = labelling.xray
    AND labelled.collection = labelling.collection
    GROUP BY labelled.xray,
             labelled.collection)
AS prelabelling
INNER JOIN (
    SELECT submission.xray                                                                     AS xray,
           submission.collection                                                               AS collection,
           GROUP_CONCAT(DISTINCT annotation.label ORDER BY annotation.label ASC SEPARATOR ',') AS labelling
    FROM ax_xray_annotation AS annotation
    INNER JOIN ax_xray_submission AS submission
    ON annotation.submission = submission.id
    WHERE submission.user = 705 -- c.hutchinson
    AND annotation.label <> 'mark'
    AND submission.submit_time >= (
        SELECT MAX(submit_time)
        FROM ax_xray_submission
        WHERE ax_xray_submission.user = submission.user
        AND ax_xray_submission.xray = submission.xray
        AND ax_xray_submission.collection = submission.collection)
    GROUP BY submission.xray,
             submission.collection)
AS postlabelling
ON prelabelling.xray = postlabelling.xray
AND prelabelling.collection = postlabelling.collection;
