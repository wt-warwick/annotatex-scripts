select CONCAT(ju.id, ',',
                ju.username, ',',
                axc.title, ',',
                axc.id, ',',
                (select count(1)
                    from ax_labelled_xray alx
                    WHERE alx.collection = axc.id), ',',
                (select count(1)
                    from (select axs.xray, axs.collection, axs.user
                            from ax_xray_submission axs
                            group by axs.xray, axs.collection, axs.user) as axs_grouped
                    where axs_grouped.collection = axc.id
                    and axs_grouped.user = ju.id
                    and axs_grouped.xray not in (select apxi.xray
                                                from ax_pending_xray as apxi
                                                where apxi.user = ju.id)), ',',
                (select count(1)
                    from ax_pending_xray as apx
                    where apx.user = ju.id
                    and apx.xray in (select xray from ax_labelled_xray as alx
                                        where alx.collection = axc.id))
)  AS 'user_id,username,collection_id,collection,total,completed,in_pending' 
from ax_xray_assignment as axa,
     ax_xray_collection as axc,
     jmt_users as ju,
     jmt_user_usergroup_map as juum
where axc.id = axa.collection 
and axa.user = ju.id
and ju.id = juum.user_id
and juum.group_id = 10
order by axc.id, ju.id;

