SELECT DISTINCT jmt_users.name,
                ax_annotation_label.label,
                ax_annotation.sentence
FROM ax_submission
INNER JOIN ax_annotation
ON ax_submission.id = ax_annotation.submission
INNER JOIN ax_annotation_label
ON ax_annotation.id = ax_annotation_label.annotation
INNER JOIN jmt_users
ON ax_submission.user = jmt_users.id
WHERE ax_submission.report IN (
        SELECT report_left.id AS id
        FROM ax_report AS report_left
        INNER JOIN ax_submission AS submission_left
        ON submission_left.report = report_left.id
        INNER JOIN ax_annotation AS annotation_left
        ON annotation_left.submission = submission_left.id
        INNER JOIN ax_annotation_label AS annotation_label_left
        ON annotation_label_left.annotation = annotation_left.id
        WHERE annotation_label_left.label NOT IN (
                SELECT annotation_label_right.label
                FROM ax_submission AS submission_right
                INNER JOIN ax_annotation AS annotation_right
                ON annotation_right.submission = submission_right.id
                INNER JOIN ax_annotation_label AS annotation_label_right
                ON annotation_label_right.annotation = annotation_right.id
                WHERE submission_right.id IN (
                        SELECT id
                        FROM ax_submission AS submissions
                        WHERE submit_time >= (
                                SELECT MAX(submit_time)
                                FROM ax_submission
                                WHERE report = submissions.report
                                AND user = submissions.user))
                AND submission_left.report = submission_right.report
                AND submission_left.user <> submission_right.user)
        AND 0 < (
                SELECT COUNT(*)
                FROM ax_submission AS submission_right
                WHERE submission_left.report = submission_right.report
                AND submission_right.user <> submission_left.user)
        AND submission_left.id IN (
                SELECT id
                FROM ax_submission AS submission_right
                WHERE submit_time >= (
                        SELECT MAX(submit_time)
                        FROM ax_submission
                        WHERE report = submission_right.report
                        AND user = submission_right.user))
        GROUP BY report_left.id)
AND ax_submission.id IN (
        SELECT id
        FROM ax_submission AS submissions
        WHERE submit_time >= (
                SELECT MAX(submit_time)
                FROM ax_submission
                WHERE report = submissions.report
                AND user = submissions.user))
ORDER BY jmt_users.id,
         ax_annotation_label.label,
         ax_annotation.sentence;
