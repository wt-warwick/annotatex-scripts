SELECT CONCAT('"', user, '","', report, '","', REPLACE(sentence, '"', '""'), '","', label, '"')
FROM ax_submission AS submission
INNER JOIN ax_annotation AS annotation
ON submission.id = annotation.submission
INNER JOIN ax_annotation_label AS annotation_label
ON annotation.id = annotation_label.annotation
WHERE annotation_label.label NOT IN (
    -- 'comparison',
    -- 'possible_diagnosis',
    -- 'recommendation',
    -- 'technical_issue',
    -- 'other',
    -- 'undefined_sentence')
    'placeholder'
    )
AND submission.user NOT IN (
    699,
    700,
    701,
    702,
    706,
    707,
    717)
AND submission.`submit_time` = (
    SELECT MAX(`submit_time`)
    FROM `ax_submission`
    WHERE `user` = submission.user
    AND `report` = submission.report)
ORDER BY report,
         user,
         sentence,
         label;
