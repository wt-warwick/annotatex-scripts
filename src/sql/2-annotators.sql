SELECT COUNT(DISTINCT left_submission.report)
FROM ax_submission AS left_submission
INNER JOIN ax_submission AS right_submission
ON left_submission.report = right_submission.report
AND left_submission.user <> right_submission.user
AND left_submission.user IN (
    703, -- l.gervais-andre
    705, -- c.hutchinson
    712, -- c.lim
    713  -- i.selby
);
