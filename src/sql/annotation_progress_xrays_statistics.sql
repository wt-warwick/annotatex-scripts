SELECT user                                                                                      AS user_id,
        ax_xray_collection.title                                                                 AS collection,
        MIN(TIMESTAMPDIFF(SECOND, ax_xray_submission.load_time, ax_xray_submission.submit_time)) AS min,
        MAX(TIMESTAMPDIFF(SECOND, ax_xray_submission.load_time, ax_xray_submission.submit_time)) AS max,
        AVG(TIMESTAMPDIFF(SECOND, ax_xray_submission.load_time, ax_xray_submission.submit_time)) AS average,
        STD(TIMESTAMPDIFF(SECOND, ax_xray_submission.load_time, ax_xray_submission.submit_time)) AS dev,
        count(1) as num_images
FROM ax_xray_submission, ax_labelled_xray, ax_xray_collection
WHERE ax_labelled_xray.xray = ax_xray_submission.xray
AND ax_xray_collection.id = ax_labelled_xray.collection
AND ax_labelled_xray.collection = ax_xray_submission.collection
GROUP BY user, ax_xray_collection.id;