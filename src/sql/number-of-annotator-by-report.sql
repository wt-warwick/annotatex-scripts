UPDATE ax_labelled_xray AS labelled_xray
INNER JOIN (
    SELECT report.id                       AS report,
           COUNT(DISTINCT submission.user) AS report_annotators
    FROM ax_submission AS submission
    INNER JOIN ax_report AS report
    ON submission.report = report.id
    INNER JOIN ax_collection_report AS collection_report
    ON collection_report.report = report.id
    WHERE collection_report.collection = 5
    AND DATE(submission.submit_time) <= '2020-01-22'
    GROUP BY report.id
    ORDER BY report.id)
AS counting
ON labelled_xray.xray = counting.report
SET labelled_xray.report_annotators = counting.report_annotators;
