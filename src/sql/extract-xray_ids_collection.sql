SELECT CONCAT(alx.xray, ',', 
              alx.collection, ',',
              axc.title)
AS 'xray_id,collection_id,collection_title'
FROM ax_labelled_xray as alx, ax_xray_collection as axc
WHERE alx.collection = axc.id;
