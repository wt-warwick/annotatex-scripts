SELECT CONCAT(submission.xray,  '$',
              submission.user,  '$',
              submission.collection, '$',
	          submission.id,    '$',
              annotation.label, '$',
	          'BB',             '$',
              annotation.shapes)
AS 'xray_id$radiologist_id$collection_id$submission_id$label$ROI_type$ROI_shape'
FROM ax_xray_annotation AS annotation
INNER JOIN ax_xray_submission AS submission
ON annotation.submission = submission.id
INNER JOIN jmt_user_usergroup_map AS usergroup_map
ON submission.user = usergroup_map.user_id
WHERE usergroup_map.group_id = 10 -- This is the 'Radiologist' group
AND submission.id >= (
    SELECT MAX(id)
    FROM ax_xray_submission
    WHERE ax_xray_submission.user = submission.user
    AND ax_xray_submission.xray = submission.xray
    AND ax_xray_submission.collection = submission.collection)
AND submission.xray not in (select xray from ax_pending_xray)
UNION
SELECT CONCAT(submission.xray,  '$',
              submission.user,  '$',
              submission.collection, '$',
	          submission.id,    '$',
              '', '$',
	          '', '$',
              '')
AS 'xray_id$radiologist_id$collection_id$submission_id$label$ROI_type$ROI_shape'
FROM ax_xray_submission AS submission
INNER JOIN jmt_user_usergroup_map AS usergroup_map
ON submission.user = usergroup_map.user_id
WHERE usergroup_map.group_id = 10 -- This is the 'Radiologist' group
AND submission.id not in (select submission from ax_xray_annotation)
AND submission.id >= (
    SELECT MAX(id)
    FROM ax_xray_submission
    WHERE ax_xray_submission.user = submission.user
    AND ax_xray_submission.xray = submission.xray
    AND ax_xray_submission.collection = submission.collection)
AND submission.xray not in (select xray from ax_pending_xray);

