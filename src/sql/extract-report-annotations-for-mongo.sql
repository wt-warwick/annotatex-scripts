SELECT CONCAT(report,        '$',
              user,          '$',
	      submission.id, '$',
	      sentence,      '$',
	      label)
AS 'report_id$radiologist_id$submission_id$sentence$label'
FROM ax_submission AS submission
INNER JOIN ax_annotation AS annotation
ON submission.id = annotation.submission
INNER JOIN ax_annotation_label AS annotation_label
ON annotation.id = annotation_label.annotation
INNER JOIN jmt_user_usergroup_map AS usergroup_map
ON submission.user = usergroup_map.user_id
WHERE usergroup_map.group_id = 10 -- This is the 'Ragiologist' group
AND submission.`submit_time` = (
    SELECT MAX(`submit_time`)
    FROM `ax_submission`
    WHERE `user` = submission.user
    AND `report` = submission.report)
ORDER BY report,
         user,
	 submission.id,
         sentence,
         label;
