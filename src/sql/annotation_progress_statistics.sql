SELECT  sessions.username AS usernam,
        DATE_FORMAT(sessions.login_action, '%Y-%m-%d') AS date,
        TIMESTAMPDIFF(SECOND, sessions.login_action, sessions.last_action) AS duration,
        TIME(sessions.login_action)                                        AS login_time,
        TIME(sessions.last_action)                                         AS last_action_time,
        sessions.submissions                                               AS submissions
FROM (
    SELECT  jmt_users.username AS username,
            jmt_action_logs.log_date        AS login_action,
            MAX(ax_submission.submit_time) AS last_action,
            COUNT(*)                       AS submissions
    FROM ax_submission
    INNER JOIN jmt_action_logs
    ON ax_submission.login = jmt_action_logs.id
    INNER JOIN jmt_users
    ON ax_submission.user = jmt_users.id
    -- WHERE ax_submission.user = ${user_id}
    GROUP BY jmt_action_logs.log_date)
AS sessions
ORDER BY username, date DESC, login_time DESC