const { exec } = require('child_process');
const fs = require('fs');
var file;
const params = process.argv.slice(2);
const parse = require('csv-parse');
const parser = parse({
    delimiter: '$',
});
const values = [];
const collections = [];

file = params[0];
collection = params[1];

if (!fs.existsSync(file)) {
    console.err('File does not exist');
    process.exit(1);
}

if (!collection) {
    console.err('Provide a collection id');
    process.exit(1);
}

fs
    .createReadStream(file)
    .pipe(parser)
    .on('data', row => {
	const id = row[0];
	const content = row[1];

	values.push('("' + id + '", "' + content.split('"').join('\\"') + '")');
	collections.push('(' + collection + ', "' + id +  '")');
    })
    .on('end', () => {
	for (let i = 0; i < values.length; i++) {
	    console.log('INSERT INTO `ax_report` (`id`, `content`) VALUES ');
	    console.log(values[i] + '; ');

	    console.log('INSERT INTO `ax_collection_report` (`collection`, `report`) VALUES ');
	    console.log(collections[i] + ';');
	}
    });
