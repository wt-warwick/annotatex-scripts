INSERT INTO ax_xray_collection (id, title, description, create_date)
SELECT MAX(id) + 1                                AS id,
       'RAND_1K_ABNORMAL_2_ABNORMAL'              AS title,
       'Abnormals taken from RAND_1K_ABNORMAL_2.' AS description,
       NOW()                                      AS create_date
FROM ax_xray_collection;
