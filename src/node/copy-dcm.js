const { execSync } = require('child_process');
var commands = [];
var file;
const fs = require('fs');
const params = process.argv.slice(2);
const parse = require('csv-parse');
const parser = parse({
    delimiter: '$',
});

file = params[0];

fs
    .createReadStream(file)
    .pipe(parser)
    .on('data', row => {
	const id = row[0];
	const source = row[3];
	const dest = 'annotatex:/var/www/html/live/media/dicoms/' + id + '.dcm';

	commands.push('scp ' + source + ' ' + dest);
    }).on('end', function() {
	for (let i = 0; i < commands.length; i++) {
	    console.log(commands[i]);
	    execSync(commands[i]);
	}
    });
