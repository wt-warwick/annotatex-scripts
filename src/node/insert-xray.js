const { exec } = require('child_process');
const fs = require('fs');
var file;
const params = process.argv.slice(2);
const parse = require('csv-parse');
const parser = parse({
    delimiter: '$',
});
const values = [];

file = params[0];

if (!fs.existsSync(file)) {
    console.err('File does not exist');
    process.exit(1);
}

fs
    .createReadStream(file)
    .pipe(parser)
    .on('data', row => {
	const id = row[0];
	values.push('("' + id + '", "/media/dicoms/' + id + '.dcm")');
    })
    .on('end', () => {
	console.log('INSERT INTO `ax_xray` (`id`, `image_path`) VALUES ');

	for (let i = 0; i < values.length - 1; i++) {
	    console.log(values[i] + ', ');
	}

	console.log(values[values.length - 1] + ';');
    });
