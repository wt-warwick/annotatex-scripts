/* This is a temporary file and it was run only once.
After uploading an image to AnnotateX, we have to insert in the table ax_labelled_xray the annotations generated for the corresponding report.
However, for xray collections 14 and 15 we skipped this step, because the images had to be annotated without any info from the report.
For xrays collections 16 and 17 we followed a different pipeline because their reports were not annotated at the time of uploading the images
*/
const { exec } = require('child_process');
const fs = require('fs');
var file;
const params = process.argv.slice(2);
const parse = require('csv-parse');
const parser = parse({
    delimiter: '$',
});
const values = [];

file = params[0];

if (!fs.existsSync(file)) {
    console.err('File does not exist');
    process.exit(1);
}

fs
    .createReadStream(file)
    .pipe(parser)
    .on('data', row => {
	const id = row[0];
	values.push('("' + id + '", 15, 0)');
    })
    .on('end', () => {
    console.log('INSERT INTO `ax_labelled_xray` (`xray`, `collection`, `report_annotators`) VALUES ');

	for (let i = 0; i < values.length - 1; i++) {
	    console.log(values[i] + ', ');
	}

	console.log(values[values.length - 1] + ';');
    });
