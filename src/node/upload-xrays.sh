#!/bin/bash

FILE=

function main {
    parse "$@"

    if [ -z "$FILE" ]; then
	echo "Must supply -i <csv>"
    fi
    
    nodejs copy-dcm.js "$FILE"
    nodejs insert-xray.js "$FILE" | ssh annotatex "mysql dbjoomla -uannotate_user -pAnnChestXray19"
}

function parse {
    while getopts c:i: ARG; do
	case $ARG in
	    i)
		FILE=$OPTARG
		;;
	    ?)
	        exit 1
	esac;
    done;
}

main "$@"
